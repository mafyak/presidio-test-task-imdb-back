package com.presidio.imdbtask.entity;

public enum UserType {

    USER, ADMIN;

    public static final String USER_ROLE = "USER";
    public static final String ADMIN_ROLE = "ADMIN";
}
