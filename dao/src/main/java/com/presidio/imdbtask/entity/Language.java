package com.presidio.imdbtask.entity;

public enum Language {

    English, Spanish, Japanese
}
