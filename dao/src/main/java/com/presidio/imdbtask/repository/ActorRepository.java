package com.presidio.imdbtask.repository;

import com.presidio.imdbtask.entity.Actor;
import org.springframework.data.repository.CrudRepository;

public interface ActorRepository extends CrudRepository<Actor, Long> {
}
