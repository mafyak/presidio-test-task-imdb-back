package com.presidio.imdbtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImdbTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImdbTaskApplication.class, args);
	}

}
