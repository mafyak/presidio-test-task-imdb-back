package com.presidio.imdbtask.service;

public interface DataGenerator {

    void generate();
}
