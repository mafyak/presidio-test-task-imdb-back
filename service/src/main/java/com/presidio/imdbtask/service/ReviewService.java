package com.presidio.imdbtask.service;

import com.presidio.imdbtask.entity.Review;

public interface ReviewService extends CrudService<Review, Long>{
}
