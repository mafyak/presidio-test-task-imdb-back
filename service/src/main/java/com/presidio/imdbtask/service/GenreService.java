package com.presidio.imdbtask.service;

import com.presidio.imdbtask.entity.Genre;

public interface GenreService extends CrudService<Genre, Long> {
}
