package com.presidio.imdbtask.service;

import com.presidio.imdbtask.entity.Actor;

public interface ActorService extends CrudService<Actor, Long> {
}
