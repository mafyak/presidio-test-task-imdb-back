package com.presidio.imdbtask.dto;

import lombok.Data;

@Data
public class ActorShortDto {

    private Long id;
    private String fullName;
    private String image;
}
