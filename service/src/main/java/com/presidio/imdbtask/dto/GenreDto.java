package com.presidio.imdbtask.dto;

import lombok.Data;

@Data
public class GenreDto {

    private Long id;
    private String name;
}
