INSERT INTO users (type, pass, email, FULL_NAME, avatar) VALUES
  (1, '$2y$12$dlJft4GUq/FQHIMSTSYOGO1EGZHM3OtwTP33C2mXmbHGYRmCWjXZy', 'admin@me.com', 'Admin Uno', 'https://m.media-amazon.com/images/M/MV5BMTY2NTg3ODIwNl5BMl5BanBnXkFtZTYwODY1Mzc3._V1_.jpg'),
  (0, '$2y$12$dlJft4GUq/FQHIMSTSYOGO1EGZHM3OtwTP33C2mXmbHGYRmCWjXZy', 'user@me.com', 'User Uno', 'https://m.media-amazon.com/images/M/MV5BMTY2NTg3ODIwNl5BMl5BanBnXkFtZTYwODY1Mzc3._V1_.jpg');

